import sys
import os
import traceback
from palworld_save_tools.commands.convert import convert_sav_to_json
from reformat import produceDataJson
from ftpService import ftpPull
from datetime import datetime


def convert():
    for root, dirs, files in os.walk("./resources"):
        for filename in files:
            if filename.endswith(".sav") :
                print(filename)
                convert_sav_to_json("./resources/" + filename, "./resources/" + filename + ".json", force=True, minify=True) #, custom_properties_keys=[".worldSaveData.CharacterSaveParameterMap.Value.RawData"])

def updateData():
    try :
        print("start ftpPull: "  + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
        ftpPull()
        print("start convert: "  + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
        convert()
        print("start produceDataJson: "  + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
        produceDataJson()
        print("end of process: "  + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    except:
        traceback.print_exc() 


def main():
    print("Running pull Main")
    updateData()


if __name__ == '__main__':
    main()