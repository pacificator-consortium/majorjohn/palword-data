import json
from treelib import Node, Tree

def showChild(json, level, result):
    if(type(json) is dict) :
        keys = json.keys()
        s = ""
        for x in keys :
            s += x + "(" + type(json[x]).__name__ + "), "
        log = level + " [" + s + "]"
        if log not in result :
            result.append(log)
        for i in keys :
            # tree.create_node(i, level)
            showChild(json[i], level + " -> " + i, result)
    if(type(json) is list and json) :
        # showChild(json[0], level + "[List]", result)
        for y in json :
            showChild(y, level + "[List]", result)


def main():
    print("Running showJsonTree Main")
    # f = open('./resources/72E642EB000000000000000000000000.sav.json')
    f = open('./resources/Level.sav.json')
    result = []
    data = json.load(f)
    print("loadfile ok")
    showChild(data, "main", result)
    for x in result :
        print(x)

if __name__ == '__main__':
    main()