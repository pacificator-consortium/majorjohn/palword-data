from ftplib import FTP
import json

def ftpPull():
    configFile = open('./config/ftp.json')
    config = json.load(configFile)

    if not config["host"] or not config["user"] or not config["password"]:
        print("Some field of ftp.json are empty")
        raise

    ftp = FTP(config["host"], config["user"], config["password"])
    ftp.cwd("/palworld - verygames/Pal/Saved/SaveGames/0/8138D0812A954941A670BE4551237DB5")

    with open("./resources/Level.sav", 'wb') as fp:
        ftp.retrbinary("RETR Level.sav", fp.write)

    ftp.cwd("Players")
    fileList = ftp.nlst()
    for f in fileList :
        with open("./resources/" + f, 'wb') as fp:
            ftp.retrbinary("RETR " + f, fp.write)
    ftp.quit()

def main():
    print("Running ftpService Main")
    ftpPull()


if __name__ == '__main__':
    main()