import json
import os
from datetime import datetime

def aggregateLevelData(outputJson):
    filePath = './resources/Level.sav.json'
    print("Open : " + filePath)
    with open(filePath) as fp:
        data = json.load(fp)
        print("Load file ok")
    outputJson["CharacterSaveParameterMap"] = data["properties"]["worldSaveData"]["value"]["CharacterSaveParameterMap"]

def aggregatePlayerData(outputJson):
    for root, dirs, files in os.walk('./resources'):
        for filename in files:
            if filename.endswith(".sav.json") and filename != "Level.sav.json" :
                filePath = './resources/' + filename
                print("Open : " + filePath)
                with open(filePath) as fp:
                    data = json.load(fp)
                    print("Load file ok")
                x = filename.split('.')[0]
                print("Dict key: " + x)
                outputJson[x]=data

def produceDataJson():
    output = {}
    aggregateLevelData(output)
    aggregatePlayerData(output)
    output["date"] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")

    with open('./resources/Data.json', "w") as fp:
        # fp.write(json.dumps(output, indent=2))
        fp.write(json.dumps(output))
        

def main():
    print("Running reformat Main")
    produceDataJson()



if __name__ == '__main__':
    main()