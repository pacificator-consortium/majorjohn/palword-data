from flask import Flask, request, jsonify
import json
from waitress import serve
from pull import updateData
from datetime import datetime

app = Flask(__name__)

@app.route("/")
def description_root():
    return '{ "desc": "Draftcorporation Service for palword-data usage."}'


@app.route("/static/uwu")
def static_uwu():
    with open('./resources/72E642EB000000000000000000000000.sav.json') as fp:
        data = json.load(fp)
    return data

@app.route("/data")
def minified_data():
    with open('./resources/Data.json') as fp:
        data = json.load(fp)
    return data

@app.route("/pull")
def update_data():
    startDate = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    updateData()
    stopDate = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    output = {}
    output["startDate"] = startDate
    output["endDate"] = stopDate
    return json.dumps(output, indent=2)

if __name__ == "__main__":
    serve(app, host="0.0.0.0", port=5000)

