# Palword data

Service pour exposer des données json d'un serveur palworld

Requière: 
Python 3.12
Pip

Packet pip nécessaire :
- Cf requirements.txt


Création d'un venv :
```
python -m venv venv
```

Activation du venv :
```
source venv/Scripts/activate
```

Afficher les packet :
```
pip freeze
```

Installer des packet requis:
```
pip install -r requirements.txt
```

Installer des packet :
```
pip install [package-name]
```


Lancer le serveur flask :
```
flask --app project/demo run
```

Lancer le serveur avec waitress :
```
python project/demo.py
```

## Installation avec Docker

Pour la compilation :
```
docker-compose build
```

Pour l'execution :
```
docker-compose up
```

Pour l'execution sur le server :
```
docker-compose up --detach
```

Pour stop l'excution sur le server :
```
docker-compose down
```




# Bug


# To Do List:  

# [X.X.X]


# [0.0.1]
 - [X] Filtrer les data
 - [ ] Endpoint par joueur
 - [ ] Auto pull du FTP tout les X minutes
 - [ ] Conversion auto du sav -> json

# [0.0.0]
 - [X] Exposer les data en brut